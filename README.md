
快速整合身份認證和密碼政䇿介紹
===

## 1. 版本

| 版本      |    描述 | Actor  | 日期  |
| :-------- | :-------- | :--: | :--: |
| v0.0.1 | 初稿|  Marco |  2020.05.16 |

## 2. 目錄

<!-- TOC depthFrom:2 -->

- [1. 版本](#1-版本)
- [2. 目錄](#2-目錄)
- [3. 介紹](#3-介紹)
    - [3.1. 結構](#31-結構)
    - [3.2. 演示](#32-演示)
- [4. 安裝](#4-安裝)
    - [4.1. 設定SMTP](#41-設定smtp)
    - [4.2. 模擬G-TV用戶注冊](#42-模擬g-tv用戶注冊)
    - [4.3. 模擬G-TV用戶登入](#43-模擬g-tv用戶登入)
- [5. Keycloak設定詳細介紹](#5-keycloak設定詳細介紹)
    - [5.1. Realm Settings設定](#51-realm-settings設定)
        - [5.1.1. 勾選常用登入功能(`重點`)](#511-勾選常用登入功能重點)
        - [5.1.2. 設定SMTP箱箱](#512-設定smtp箱箱)
        - [5.1.3. 自定主題(`重點`)](#513-自定主題重點)
            - [5.1.3.1. 建立新主題前先停用主題緩存](#5131-建立新主題前先停用主題緩存)
            - [5.1.3.2. 創建新主題](#5132-創建新主題)
            - [5.1.3.3. 套用新主題](#5133-套用新主題)
            - [5.1.3.4. 修改主題外觀](#5134-修改主題外觀)
            - [5.1.3.5. 啟用主題緩存](#5135-啟用主題緩存)
        - [5.1.4. 調節Tokens生命周期](#514-調節tokens生命周期)
        - [5.1.5. 調節安全防禦(`重點`)](#515-調節安全防禦重點)
    - [5.2. Roles 設定](#52-roles-設定)
    - [5.3. Client Scopes 設定](#53-client-scopes-設定)
    - [5.4. Clients 設定(`重點`)](#54-clients-設定重點)
    - [5.5. Authentication 設定](#55-authentication-設定)
        - [5.5.1. Required Actions 設定](#551-required-actions-設定)
        - [5.5.2. Password Policy 密碼政䇿設定(`重點`)](#552-password-policy-密碼政䇿設定重點)
        - [5.5.3. OTP Policy 設定](#553-otp-policy-設定)
    - [5.6. Groups 設定](#56-groups-設定)
    - [5.7. Users 設定](#57-users-設定)
    - [5.8. Export 匯出設定檔(`重點`)](#58-export-匯出設定檔重點)
    - [5.9. 寫入docker compose發佈檔(`重點`)](#59-寫入docker-compose發佈檔重點)
- [6. Glossary 詞彙表](#6-glossary-詞彙表)

<!-- /TOC -->


## 3. 介紹

此文主要講述有關快速整合G平台身份識別、訪問管理和登入安全的**介紹**和**演示**

### 3.1. 結構

![](out/assets/uml/components.svg)

### 3.2. 演示

**代碼主要實現以下功能**

- 前端部份
    - 創建Hello World并設置OpenID Connect鏈接
- Keycloak部份
    - Realm Settings設定
        - 勾選常用登入功能(`重點`)
        - 設定SMTP郵箱
        - 自定主題(`重點`)
        - 調節Tokens生命周期
        - 調節安全防禦(`重點`)
    - Roles 設定
    - Client Scopes 設定
    - Clients 設定(`重點`)
    - Authentication 設定
        - Required Actions 設定
        - Password Policy 密碼政䇿設定(`重點`)
        - OTP Policy 設定
    - Groups 設定
    - Users 設定
    - Export 匯出設定檔(`重點`)
- 寫入docker compose發佈檔(`重點`)
- 一鍵安裝

## 4. 安裝

演示代碼展示如果用一個以Vue寫的前端接入Keycloak

**運行環境**

- Mac/Linux機
- 需要安裝Git, Node和[Docker](https://docs.docker.com/docker-for-mac/install/)

**下載與安裝**

```bash
git clone https://gtvssodemo@bitbucket.org/gtvssodemo/iam-demo.git

cd iam-demo

docker-compose up -d
```

### 4.1. 設定SMTP

以超級管理員進入`Master`域，帳號/密碼：admin/Adm@i19j_1d

http://localhost:9080

![](assets/images/2020-05-17-20-45-08.png)

設定SMTP: http://localhost:9080/auth/admin/master/console/#/realms/gtv/smtp-settings

測試SMTP帳號/密碼：gtv.sso.demo@gmail.com/mtzotwzwfjugbrgb

![](assets/images/2020-05-17-20-47-52.png)


### 4.2. 模擬G-TV用戶注冊

進入`G-TV`域模擬用戶注冊

http://localhost:9080/auth/admin/gtv/console

![](assets/images/2020-05-17-20-36-24.png)

![](assets/images/2020-05-17-20-36-53.png)

确認郵箱

![](assets/images/2020-05-17-20-52-34.png)

### 4.3. 模擬G-TV用戶登入

```bash
cd gtv

npm install

npm run serve
```

> **TIPS:**
> 參考: https://github.com/dsb-norge/vue-keycloak-js/tree/master/examples/hello-keycloak

![](assets/images/2020-05-17-23-12-56.png)

打開網址 http://localhost:8081/

![](assets/images/2020-05-17-23-13-37.png)

![](assets/images/2020-05-17-23-23-52.png)



## 5. Keycloak設定詳細介紹 

**此章節所有的Keycloak設定已匯出成設定檔，可一鍵部署，設定檔存放於**
- iam/realm-config/gtv-realm.json
- iam/themes/gtv


**登入`Master`域**

> **TIPS:**
> `Master`域一般不對外開放，只提供給超級管理員管理所有域(Realm)和創建新域用

http://localhost:9080

![](assets/images/2020-05-16-14-01-40.png)

點"Administration Console"

![](assets/images/2020-05-16-14-02-28.png)

帳號/密碼：`admin/Adm@i19j_1d`

![](assets/images/2020-05-16-14-05-16.png)

### 5.1. Realm Settings設定

建立新域(Realm)，每個域相當於共享單點登入(SSO)，如G-TV，G-News，G-Fashion應連同一個域，這裡創建時直接把Realm名子命名為gtv

![](assets/images/2020-05-16-14-04-16.png)


#### 5.1.1. 勾選常用登入功能(`重點`)

![](assets/images/2020-05-16-14-06-33.png)

查看效果：http://localhost:9080/auth/realms/gtv/console

![](assets/images/2020-05-16-14-36-49.png)

#### 5.1.2. 設定SMTP箱箱

![](assets/images/2020-05-16-14-22-53.png)

查看測試郵件

![](assets/images/2020-05-16-14-24-09.png)

#### 5.1.3. 自定主題(`重點`)

預設有base和keycloak主題，若想修改整個外觀和感覺請繼承base主題修改，或複制一份後改名。如果只想作少量修改一般extend keycloak就可以。

![](assets/images/2020-05-16-14-25-09.png)

例子: 把原有的base主題複制出來，展示修改login模板，改為G-TV主題

##### 5.1.3.1. 建立新主題前先停用主題緩存

```bash
docker cp iam:/opt/jboss/keycloak/standalone/configuration/standalone.xml .

vim standalone.xml
```

```xml
<theme>
    <staticMaxAge>-1</staticMaxAge>
    <cacheThemes>false</cacheThemes>
    <cacheTemplates>false</cacheTemplates>
    ...
</theme>
```

```bash
docker cp standalone.xml iam:/opt/jboss/keycloak/standalone/configuration/

docker restart iam
```

##### 5.1.3.2. 創建新主題

<!--
docker cp iam:/opt/jboss/keycloak/themes/base/account/messages/messages_en.properties themes/gtv/account

docker cp iam:/opt/jboss/keycloak/themes/base/account/messages/messages_zh_CN.properties themes/gtv/account

docker cp iam:/opt/jboss/keycloak/themes/base/admin/messages/messages_en.properties themes/gtv/admin

docker cp iam:/opt/jboss/keycloak/themes/base/admin/messages/messages_zh_CN.properties themes/gtv/admin

docker cp iam:/opt/jboss/keycloak/themes/base/email/messages/messages_en.properties themes/gtv/email

docker cp iam:/opt/jboss/keycloak/themes/base/email/messages/messages_zh_CN.properties themes/gtv/email

docker cp iam:/opt/jboss/keycloak/themes/base/login/messages/messages_en.properties themes/gtv/login

docker cp iam:/opt/jboss/keycloak/themes/base/login/messages/messages_zh_CN.properties themes/gtv/login
-->

```bash
mkdir -p themes/gtv/login/messages

# 增加支持繁體中文(Keycloak預設支持17种語言，但沒有繁體中文)，支持簡繁英。
docker cp iam:/opt/jboss/keycloak/themes/base/login/messages/messages_en.properties themes/gtv/login/messages

docker cp iam:/opt/jboss/keycloak/themes/base/login/messages/messages_zh_CN.properties themes/gtv/login/messages

cp themes/gtv/login/messages/messages_zh_CN.properties themes/gtv/login/messages/messages_zh_TW.properties
```

<!-- https://github.com/eclipse/che/tree/master/dockerfiles/keycloak/che -->

> **TIPS:**
> 需要手動在每個messages properties加上locale_zh-TW=中文繁体，不然切換語言時只會出現"zh-TW"代替文字"中文繁体"

```bash
# 建立新主題設定文件，支持簡繁英
vim themes/gtv/login/theme.properties
```

```ini
parent=base
import=common/keycloak
locales=zh-CN,zh_TW,en
```

> **TIPS**
> account,admin,email主題同上。必須同時套用login,account,admin和email主題才能選新增的語言"zh-TW"

##### 5.1.3.3. 套用新主題

```bash
# 複制新主題到Keycloak主題目錄
docker cp themes/gtv iam:/opt/jboss/keycloak/themes
```

![](assets/images/2020-05-16-17-23-21.png)

查看`GTV`域之效果: http://localhost:9080/auth/admin/gtv/console

![](assets/images/2020-05-16-17-30-22.png)

##### 5.1.3.4. 修改主題外觀

由於新主題只有空白外觀，這裡隨意在網絡找到一個主題Eclipse che作只作少量修再進一步演示，主題網址https://github.com/eclipse/che/tree/master/dockerfiles/keycloak/che，下載後把相關文件放到themes/gtv下，套用并修改後效果如下

**登入界面：**
![](assets/images/2020-05-16-19-46-06.png)

**注冊界面：**
![](assets/images/2020-05-16-19-46-59.png)

![](assets/images/2020-05-16-19-51-23.png)

**忘記密碼界面：**
![](assets/images/2020-05-16-19-49-30.png)

**驗證郵件界面**
![](assets/images/2020-05-16-19-53-09.png)

##### 5.1.3.5. 啟用主題緩存

主題建立好後，主題緩存請回覆到預設狀態，預設是啟用

#### 5.1.4. 調節Tokens生命周期

![](assets/images/2020-05-16-20-03-06.png)

#### 5.1.5. 調節安全防禦(`重點`)

**配置Headers**

預設配置如下，可自行配置

![](assets/images/2020-05-16-20-17-29.png)

**配置暴力破解密鑰**

![](assets/images/2020-05-16-20-14-50.png)

### 5.2. Roles 設定

預設Roles 設定如下:

![](assets/images/2020-05-17-14-53-09.png)

這裡我們創建兩個新Role

- ROLE_ADMIN： G平台的內容管理者，此用戶不是超級管理員，一般不充許權限進入IAM (Keycloak)
- ROLE_USER：G平台的普通用戶

![](assets/images/2020-05-17-20-20-17.png)

![](assets/images/2020-05-17-14-58-32.png)

> **TIPS:**
> 以上是Realm roles，Keycloak分為Realm roles尸和Client roles
> Realm roles位於所有客戶端共享的全局命名空間中
> Client roles基本上具有專用於客戶端的名稱空間, 於5.4章節有介紹
> Realm-level roles are in global namespace shared by all clients.

### 5.3. Client Scopes 設定

Client Scope使您可以定義一組通用的協議映射器和角色，這些映射器和角色在多個客戶端之間共享

預設Client Scopes 設定如下，這些Client Scopes是Keycloak內部使用:

![](assets/images/2020-05-17-14-50-24.png)

我們建立一個新的Client Scope命名為G-Platform，以供G平台使用

![](assets/images/2020-05-17-15-13-22.png)

Mappers是將用戶屬性，角色等映射到Token中。
以下建立3個Mappers，分別為login, roles和langKey。其中roles點選`Add to access token`以方便login後decode token即識別不同role跳到不同面頁。

![](assets/images/2020-05-17-17-01-57.png)

![](assets/images/2020-05-17-17-13-21.png)

### 5.4. Clients 設定(`重點`)

Client是可以請求Keycloak驗證用戶身份的實體。大多數情況下，客戶端是希望使用Keycloak來保護自身並提供單一登錄解決方案的應用程序和服務。Client也可以是只想請求身份信息或訪問令牌的實體，以便它們可以安全地調用網絡上由Keycloak保護的其他服務

預設Clients如下:

![](assets/images/2020-05-17-13-44-15.png)

怎樣建Client才最合理? 一般我們會分開前端，後端，如用後端是microservice，我們還會有service discovery服務，我們以此作為例子建立3個Client，分別命名為

- **g-web** : 提供給前端
- **g-backend** : 提供給後端
- **g-registry** : 提供給Service Discovery服務

![](assets/images/2020-05-17-14-31-31.png)

![](assets/images/2020-05-17-14-32-09.png)

![](assets/images/2020-05-17-14-33-23.png)

g-web的設置如下

> **TIPS**
> 前端Client一般用grant_type: password

![](assets/images/2020-05-17-23-24-44.png)

g-backend的設置如下

> **TIPS**
> 後端Client一般用grant_type: client_credentials

![](assets/images/2020-05-17-17-47-41.png)

![](assets/images/2020-05-17-18-15-39.png)

分配G-Platform到Client
![](assets/images/2020-05-17-15-32-14.png)

> **TIPS:**
> 為方便演示，代碼只用到g-web部分，這裡把通常用到的三個Client都先建立起來以便方便實際使用

客戶端<br>Client | 啟用標準流程<br>Standard Flow Enabled | 啟用隱式<br>Implicit Flow Enabled |  啟用直接訪問授權<br>Direct Access Grants Enabled | 啟用服務帳戶<br>Service Accounts Enabled | 啟用授權<br>Authorization Enabled
---------|----------|---------|---------|---------|---------
 g-web | ON | ON |  |  | 
 g-backend |  |  | |  ON | ON 
 g-registry | ON |  | ON | |  

> **TIPS:**
> 甚麼是Standard Flow Enabled? . . ., 鼠標放於旁邊`？`號有提示，詳細請參考官方文檔
> https://www.keycloak.org/docs/latest/server_admin/#oidc-clients


模擬前端登入測試(Client ID: `g-web`)

> **TIPS:**
> 為方便postman展示，需要暫時啟用`Direct Access Grants Enabled`，方便測試工具直接調用login API，否則一般的OpenID Connect流程需要驗證Authorization Code Flow，
> 測試後需關閉

```bash
POST http://localhost:9080/auth/realms/gtv/protocol/openid-connect/token
```

![](assets/images/2020-05-17-16-13-57.png)

> **TIPS:**
> 為方面測試，於章節5.7新增了一個用戶，帳號/密碼: admin@localhost/admin，

查看JWT解碼內容，利用工具https://jwt.io/

![](assets/images/2020-05-17-16-14-43.png)

其中payload具體內容如下

```json
{
  "exp": 1589703490,
  "iat": 1589703190,
  "jti": "63e791f4-34a4-4fd4-a0a8-9840fc3389f3",
  "iss": "http://localhost:9080/auth/realms/gtv",
  "aud": "account",
  "sub": "e5d286eb-89a4-4bfb-8e79-4be45a3ab54d",
  "typ": "Bearer",
  "azp": "g-web",
  "session_state": "59b94faa-160c-499c-9b3c-9912593445df",
  "acr": "1",
  "allowed-origins": [
    "http://localhost:8080/*"
  ],
  "realm_access": {
    "roles": [
      "offline_access",
      "ROLE_ADMIN",
      "uma_authorization"
    ]
  },
  "resource_access": {
    "account": {
      "roles": [
        "manage-account",
        "manage-account-links",
        "view-profile"
      ]
    }
  },
  "scope": "profile G-Platform email",
  "email_verified": true,
  "roles": [
    "offline_access",
    "ROLE_ADMIN",
    "uma_authorization"
  ],
  "name": "大文 陳",
  "preferred_username": "admin@localhost",
  "locale": "zh-CN",
  "given_name": "大文",
  "family_name": "陳",
  "email": "admin@localhost"
}
```

> **TIPS:**
> 以上除了preferred_username(預設會顯示，值可指定)，roles為自定的key。其它json key/value都是Keycloak access token預設包括的內容。一般access token只存放少量用戶資料佢為身份識別用途并存放於IAM(Keycloak)數據庫，其它用戶資料如性別/年齡等應存放於Application之數據庫(即G-TV，G-News，G-Fashion)的資料庫，以唯一鍵preferred_username(可在Keycloak設定用email或是username作為唯一用戶ID)配對用戶。


### 5.5. Authentication 設定

為Realm配置身份驗證時，應注意一些功能。許多組織都有嚴格的密碼和OTP策略，您可以通過管理控制台中的設置來實施


#### 5.5.1. Required Actions 設定

Required Actions是用戶必須先完成的任務，然後才能被允許登錄。用戶必須Login才能執行Required Actions，完成後，用戶將無需再次Required Actions。

![](assets/images/2020-05-17-19-07-23.png)

#### 5.5.2. Password Policy 密碼政䇿設定(`重點`)

![](out/assets/uml/PasswordPolicy.svg)

預設沒有套用密碼政䇿

![](assets/images/2020-05-17-19-04-10.png)

套用後

![](assets/images/2020-05-17-19-16-52.png)

選項 | 說明
---------|----------
 Special Characters | 最少一個特殊字符
 Uppercase Characters | 最少一個大寫字母
 Lowercase Characters | 最少一個小寫字母
 Digits | 最少一個數字
 Minimum Length | 密碼長度不少于8
 Not Username | 不能含有用戶名
 Not Recently Used | 不能與之前的3 個舊密碼相同
 Hashing Algorithm| 密碼哈希算法

#### 5.5.3. OTP Policy 設定

OTP策略支持Google Authenticator和FreeOTP

![](assets/images/2020-05-17-19-24-07.png)

### 5.6. Groups 設定

Keycloak中的Groups允許您為一組用戶管理一組通用的屬性和角色映射

於gtv域手動建立2個Groups

組 | 說明
---------|----------
 Admins | 內容管理員組，非超級管理員
 Users | 普通用戶組

![](assets/images/2020-05-17-19-29-42.png)

授予ADMIN角色到Admins組

![](assets/images/2020-05-17-19-30-17.png)


授予admin用戶色到Admins組

![](assets/images/2020-05-17-19-32-22.png)

### 5.7. Users 設定

於gtv域手動建立2個帳號

用戶 | 說明
---------|----------
 admin/Admin01! | 內容管理員，非超級管理員
 user/User012! | 普通用戶

手動建立G-Platform內容管理員，帳號/密碼：admin/admin

![](assets/images/2020-05-17-16-01-54.png)

![](assets/images/2020-05-17-15-23-58.png)

![](assets/images/2020-05-17-15-22-33.png)

### 5.8. Export 匯出設定檔(`重點`)

![](assets/images/2020-05-17-19-37-14.png)

### 5.9. 寫入docker compose發佈檔(`重點`)

## 6. Glossary 詞彙表

Term | Definition
---------|----------
 Keycloak | 以最小的麻煩為應用程序和安全服務添加身份驗證。無需處理存儲用戶或認證用戶。開箱即用。
 GIT | Git是一個免費的開源分佈式版本控制系統，旨在快速高效地處理從小型到大型項目的所有內容
 Docker | Docker通過克服應用程序開發的複雜性來幫助開發人員將他們的想法變為現實
 Docker compose | Docker Compose是用於定義和運行多容器Docker應用程序的工具。通過Compose，您可以使用YAML文件來配置應用程序的服務。
 VUE |  是一套用于构建用户界面的渐进式框架。与其它大型框架不同的是，Vue 被设计为可以自底向上逐层应用。
 OpenID Connect | OpenID Connect是基於OAuth 2.0系列規範的可互操作的身份驗證協議。
 Realm (Keycloak's) | 領域管理一組用戶，憑據，角色和組。用戶屬於並登錄到領域。領域彼此隔離，並且只能管理和認證他們控制的用戶。
 SMTP | SMTP是郵件服務器通過Internet傳輸電子郵件的通信協議。 
 Brute Force Detection (BFD) | 暴力攻擊是通過系統地嘗試字母，數字和符號的每種可能的組合，直到找到一種有效的正確組合，來發現密碼的嘗試
 Password Policy | 密碼策略是一組規則，旨在通過鼓勵用戶使用強密碼並正確使用它們來增強計算機安全性。 ...一些政府擁有國家認證框架，該框架定義了用戶對政府服務的認證要求，包括密碼要求。
 OTP Policy | OTP身份驗證提供了雙重身份驗證的第二部分。 ... MAG OTP策略將接收到的OTP值與數據庫中存儲的OTP值進行比較。如果OTP有效，則授予對受保護API的訪問權限。
 IAM | 身份和訪問管理（IAM）是業務流程，策略和技術的框架，可促進電子或數字身份的管理。 ...角色是根據企業內部的工作能力，權限和責任來定義的。
 SSO | 單一登錄（SSO）是一種會話和用戶身份驗證服務，它允許用戶使用一組登錄憑據（例如，名稱和密碼）訪問多個應用程序。